use strict;
use warnings;

use Module::Build;
my $build = Module::Build->new(
    module_name          => 'Stanford::DebRepo',
    dist_abstract        => 'Download Debian Packages file and compare package versions',
    license              => 'perl',
    recursive_test_files => 1,
    requires             => {
        'perl' => '5.6.1',
    },
);

$build->create_build_script;
