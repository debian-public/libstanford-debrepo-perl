# NAME

debrepo-version-check - Test if Debian package versions in local repo are up-to-date

# USAGE

debrepo-version-check list

debrepo-version-check check &lt;package\_name1>\[,&lt;package\_name2>,...\]

debrepo-version-check --help

debrepo-version-check --manual

# DESCRIPTION

Given the name of a Debian package **debrepo-version-check** compares the
upstream package version with the local package version. If the local
version is not at least as large as the upstream version the test fails
and sends the usual Test::More output to standard output, and (optionally)
an alert will be sent to the Nagios servers specified with the
**--nagios-servers** option.

The local package version is derived by looking for the package in the
Debian repository specified by the **--local-repo-url** URL; if this option
is not supplied, a default of _http://debian.stanford.edu/debian-stanford_
is used.

**debrepo-version-check** can only do checks against a limited number of
packages; use the **list** action to see those packages that
**debrepo-version-check** can check. Attempting to check a package that
**debrepo-version-check** does not know about will result in an error.

# REQUIRED ARGUMENTS

You need to supply an action, either "**list**" or "**check**".

# EXAMPLES

To see a list of packages supported by **debrepo-version-check**:

     $ debrepo-version-check list

To check that the _terraform_ package in the local Debian repository is
at the same version as the upstream version:

     $ debrepo-version-check check terraform
     ok 1 - local version of terraform at least as large as upstream version
     1..1

To check that the _terraform_ package in the local Debian repository is
at the same version as the upstream version AND send the results to a
couple of Nagios servers:

     $ debrepo-version-check check terraform --nagios-servers 192.168.1.2,192.168.3.4

# ACTIONS

- _list_

    This action lists all the packages that debain-repo-watch can do tests
    against.

- _check_ _package\_name1,\[,package\_name2,...\]_

    To run checks use the **check** action. This action requires a second
    argument that is a comma-delimited list of package names. If all of the
    package checks success, when the script exits it will return 0, otherwise
    it will return with 1.

# OPTIONS

- **-h|--help**

    Display the help screen.

- **-m|--manual**

    Display the man page.

- **--local-repo-url** _URL of local Debian repository_

    Specify the URL to the local Debian repository to check against. If this
    option is omitted then _http://debian.stanford.edu/debian-stanford_ will
    be used.

- **--nagios-server** _server1\[,server2,...\]_

    A comma-delimited list of server addresses or hostnames. Each test result
    will be sent to these servers as a Nagios passive alert. The Nagios test
    name used will be "package &lt;package\_name> version". For example, if
    testing that the wallet-client package is up-to-date, the Nagios test name
    will be "package wallet-client version". The hostname sent in the passive
    alert will be "example.com" unless overridden by the **--nagios-hostname**
    option.

- **--nagios-hostname** _hostname_

    Nagios test results are associated with a host. By default, this script
    will use the hostname "example.com". If you want to override this, use the
    **--nagios-hostname** option.

- **--test-name-override** _test-name_

    The name of the test sent to Nagios is normally "package &lt;package\_name>
    version". If you want to change this, use the **--test-name-override**
    option. Note that if you use this option, then the test name sent will be
    the same for all packages checked, so this option is only useful when
    checking a single package.

- **--version-diff** _version-diff_

    You can specify how far the local package version can be from the upstream
    version by supplying the **--version-diff** option. For example,

        debrepo-version-check check patchman-client --version-diff '0.0.4'

    means only generate a warning if the local version of the patchman-client
    package is at least '0.0.4' versions behind the upstream version. Thus, if
    the local package has version '2.0.10' and the upstream version has
    version '2.0.12' then this is within '0.0.4' versions and no alert will be
    generated. But if, instead, the upstream version is '2.0.17' this is too
    far and an alert will be generated. This option overrides the values in
    the hash variable `%PACKAGE_TO_VERSION_DIFF` defined at the top of the
    **debrepo-version-check** executable.

    Use the string "`compare`" to alert on any difference.

- **-q|--quiet**

    Use this option to suppress all Test::More output. The exit code (0 or 1)
    will still be output as usual.

- **--verbose**

    Show more information during run.
