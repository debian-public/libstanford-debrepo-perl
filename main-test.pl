use lib q{lib};

use YAML;
use IO::File;
use Stanford::DebRepo;
use Carp;

my $DEBUG = 1 ;

## Step 1. Set up  local repo object..
my $local_url       = 'http://debian.stanford.edu/debian-stanford' ;
my $local_code      = 'buster' ;
my $local_component = 'main' ;

my $local_dr = Stanford::DebRepo->new(
        url       => $local_url,
        code      => $local_code,
        component => $local_component,
        debug     => $DEBUG,
        ) ;
my $local_package_file = $local_dr->download_packages_file();

## Step 2. Get local versions of some packages.
#    ossec-hids-server
my @packages = qw (
    wallet-client
    patchman-client
    terraform
    ) ;

my %package_to_local_version = ();
foreach my $package (@packages) {
    my @versions = $local_dr->get_versions($package) ;

    my $local_version;
    if (scalar(@versions) > 1) {
        croak "found more than version for package '$package'";
    } else {
        $local_version = $versions[0] ;
    }

    $local_dr->progress("$package: $local_version (local)");
    $package_to_local_version{$package} = $local_version;
}

my %package_to_upstream_version = ();
foreach my $package (@packages) {
    my $uv = $local_dr->get_upstream_version($package) ;
    $local_dr->progress("$package: $uv (upstream)");
    $package_to_upstream_version{$package} = $uv;
}


#my $url       = 'https://repo.openbytes.ie/debian/' ;
#my $code      = 'buster' ;
#my $component = 'main' ;

my ($uv, $package) ;

my $process_me_fref = sub  {
    my ($package) = @_ ;

    print "\nprocessing '$package':\n" ;
    my $uv = $package_to_upstream_version{$package} ;
    my $lv = $package_to_local_version{$package} ;
    print "local version:    $lv\n";
    print "upstream version: $uv\n";

    my $cmp = Stanford::DebRepo::compare_versions($lv, $uv) ;

    if ($cmp < 0) {
        print "local version '$lv' is less than upstream version '$uv'\n" ;
    } elsif ($cmp > 0) {
        print "local version '$lv' is greater than upstream version '$uv'\n" ;
    } elsif ($cmp == 0) {
        print "local version '$lv' is the same as upstream version '$uv'\n" ;
    } else {
        die "unknown compare version came back" ;
    }

} ;

$process_me_fref->('wallet-client') ;
$process_me_fref->('patchman-client') ;
$process_me_fref->('terraform') ;


####

## http://mirror.centos.org/centos/8/BaseOS/x86_64/os/repodata/
#
#my $yr_url       = 'http://mirror.centos.org/centos/8/BaseOS/x86_64/os/repodata/' ;
#my $yr_release   = '8' ;
#my $yr_component = 'BaseOS' ;
#
#my $yr = YumRepo->new(
#    url       => $yr_url,
#    release   => $yr_release,
#    component => $yr_component,
#    debug     => 1,
#) ;
