package Stanford::DebRepo;

## no critic (Modules::RequireNoMatchVarsWithUseEnglish);
## no critic (Subroutines::RequireArgUnpacking);
## no critic (CodeLayout::ProhibitParensWithBuiltins);

use strict;
use warnings;
use autodie;

# Make this a Moose class
use Moose;
use namespace::autoclean;

use Data::Dumper;
use Carp;
use English;
use LWP::Simple;
use File::Temp;
use JSON;
use File::Slurper;
use version;

use AptPkg::Config;
use Parse::Debian::Packages;

# The URL of the Debian repository.
has url  => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

# The code name (e.g., "squeeze", buster", "sid", etc.)
has code  => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

# The "component" (e.g., "main", non-free", etc.)
has component  => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

# The architecture (defaults to "binary-amd64")
has architecture => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
    default  => 'binary-amd64',
);

# The local path to the file containing the 'Packages' file.
# This will be filled in by the download_packages_file method.
has packages_path => (
    is       => 'rw',
    isa      => 'Str',
    required => 0,
);

has debug => (
    is       => 'rw',
    isa      => 'Bool',
    required => 1,
    default  => 0,
);

sub to_s {
    my $self = shift ;

    my $url           = $self->url() ;
    my $code          = $self->code() ;
    my $component     = $self->component() ;
    my $architecture  = $self->architecture() ;
    my $packages_path = $self->packages_path() ;

    if (!$packages_path) {
        $packages_path = 'NOT DEFINED' ;
    }

    return <<"END_STRING";
url:           $url
code:          $code
component:     $component
architecture:  $architecture
packages_path: $packages_path
END_STRING
}

sub progress {
    my $self = shift ;
    my ($msg) = @_ ;

    if ($self->debug()) {
        print "progress: $msg\n" ;
    }
    return ;
}

sub strip_trailing_slash {
    my ($x) = @_ ;

    $x =~ s{[\/]+$}{}gxsm;
    return $x ;
}

# Given a URL, download that URL and put into a temporary file. The
# path to that file is returned. Note that the temporary file must
# be removed manually (use the cleanup method to do this).
sub store_url_to_file {
    my $self = shift ;
    my ($url) = @_ ;

    my $file = $self->tempfile();
    # Use LWP::Simple's getstore
    my $response_code = LWP::Simple::getstore($url, $file);

    if (LWP::Simple::is_error($response_code)) {
        croak "download of '$url' failed; response code: $response_code";
    }

    return $file ;
}

# Construct the URL that points to the "Packages" file.
sub construct_packages_url {
    my $self = shift ;

    my $url          = strip_trailing_slash($self->url());
    my $code         = $self->code();
    my $component    = $self->component();
    my $architecture = $self->architecture();

    return "$url/dists/$code/$component/$architecture/Packages";
}

sub tempfile {
    my $self = shift ;
    my $fh = File::Temp->new(UNLINK => 0);
    return $fh->filename;
}

# Download a Packages file. Sets the packages_path property to the path
# of the file containing the download.
sub download_packages_file {
    my $self = shift ;

    $self->progress('about to download the Packages file') ;

    my $url      = $self->construct_packages_url() ;
    my $filepath = $self->store_url_to_file($url);

    $self->progress("path to downloaded file: $filepath") ;

    # Store the file path in the packages_path property.
    $self->packages_path($filepath) ;

    # Parse the file path.
    return $filepath ;
}

# Given a package name parse the Packages file pointed to by the
# packages_path property and find the version(s) of that package in that
# Packages file. If the packages_path is not defined or points to a
# non-existent file, the method dies.
sub get_versions {
    my $self = shift ;
    my ($package_name) = @_ ;

    if (!$package_name) {
        croak 'missing package_name parameter' ;
    }

    my $packages_path = $self->packages_path() ;
    if (!$packages_path || (! -f $packages_path)) {
       croak 'missing packages file; did you remember to download it?' ;
    }

    $self->progress("getting version for package '$package_name'") ;

    my $fh = IO::File->new($self->packages_path());
    my $parser = Parse::Debian::Packages->new($fh);
    my @versions = () ;
    while (my %package = $parser->next) {
        my $cur_package = $package{'Package'} ;
        if ($cur_package eq $package_name) {
            my $version = $package{'Version'} ;
            $self->progress("found a version: $version");
            push(@versions, $self->source_version($version)) ;
        }
    }

    return @versions ;
}

sub cleanup {
    my $self = shift ;

    my $packages_path = $self->packages_path() ;
    if ($packages_path && (-f $packages_path)) {
        $self->progress("deleting Packages file '$packages_path'") ;
        unlink $packages_path ;
    } else {
        $self->progress("no need to delete Packages file '$packages_path' (not there)") ;
    }

    return ;
}

# Strip off everthing past the source version. For example:
#
# 1.2.12-3 --> 1.2.12
sub source_version {
    my $self = shift ;
    my ($version) = @_ ;

    $version =~ s{^([^-]+)[-]{0,1}.*$}{$1}xsm;
    return $version ;
}

sub get_upstream_version {
    my $self = shift ;
    my ($package_name) = @_ ;

    # The get_uv_* functions return die if no version was found, so be sure to
    # to trap this function with an eval.

      $package_name eq 'patchman-client'   ? return $self->get_uv_patchman_client()
    : $package_name eq 'ossec-hids-server' ? return $self->get_uv_ossec()
    : $package_name eq 'terraform'         ? return $self->get_terraform()
    : $package_name eq 'wallet-client'     ? return $self->get_uv_wallet_client()
    : croak "unrecognized package '$package_name'";

    return ;
}

sub get_terraform {
    my $self = shift ;

    my $url = 'https://releases.hashicorp.com/terraform/';
    my $releases_file = $self->store_url_to_file($url) ;

    # Look for the most recent release.
    ## no critic (InputOutput::RequireBriefOpen);
    open(my $FH, '<', $releases_file);
    my $cur_max_version = '0.0.0' ;
    while (my $cur_line = <$FH>) {
        if ($cur_line =~ m{terraform_(\d+[.]\d+[.]\d+)[^\-\d]}xsm) {
            my $version = $1 ;
            $self->progress("found terraform version $version");
            if (version->parse($cur_max_version) < version->parse($version)) {
                $cur_max_version = $version ;
                $self->progress("max version is currently $cur_max_version");
            }
        }
    }
    close($FH);

    if ($cur_max_version eq '0.0.0') {
        return undef ;
    } else {
        return $cur_max_version ;
    }
}

# Get the upstream version of the patchman client. We do this
sub get_uv_patchman_client {
    my $self = shift ;

    my $latest_release = $self->github_latest_release('furlongm', 'patchman');
    $self->progress("latest release for patchman-client is $latest_release");

    # Remove the leading "v".
    $latest_release =~ s{^v}{}xsm;

    return $self->source_version($latest_release) ;
}

sub get_uv_ossec {
    my $self = shift ;

    my $ossec_dr = Stanford::DebRepo->new(
        url       => 'https://updates.atomicorp.com/channels/ossec/debian',
        code      => $self->code(),
        component => $self->component(),
        debug     => $self->debug(),
        ) ;

    my $local_package_file = $ossec_dr->download_packages_file();
    print $ossec_dr->to_s();
    my $x = $ossec_dr->packages_path();

    my @versions = $ossec_dr->get_versions('ossec-hids-server') ;

    if (scalar(@versions) > 1) {
        croak 'more than one version of ossec-hids-server';
    } elsif (scalar(@versions) == 1) {
        my $version = $versions[0] ;
        $self->progress("found version for ossec-hids-server: $version");
        return $version;
    } else {
        $self->progress('WARNING!! found NO versions for ossec-hids-server');
        return ;
    }
}

sub get_uv_wallet_client {
    my $self = shift ;

    my $package = 'wallet-client' ;

    my $ossec_dr = Stanford::DebRepo->new(
        url       => 'https://archives.eyrie.org/debian',
        code      => $self->code(),
        component => $self->component(),
        debug     => $self->debug(),
        ) ;

    my $local_package_file = $ossec_dr->download_packages_file();
    #print $ossec_dr->to_s();
    my $x = $ossec_dr->packages_path();
    #print `cat $x` ;

    my @versions = $ossec_dr->get_versions($package) ;

    if (scalar(@versions) > 1) {
        croak "more than one version of $package";
    } elsif (scalar(@versions) == 1) {
        my $version = $versions[0] ;
        $self->progress("found version for $package: $version");
        return $version;
    } else {
        $self->progress("WARNING!! found NO versions for $package");
        return ;
    }
}

sub github_latest_release {
    my $self = shift ;
    my ($owner, $repo) = @_ ;

    my $url  = "https://api.github.com/repos/${owner}/${repo}/releases/latest" ;
    $self->progress("GitHub latest release url is '$url'");

    my $latest_json = $self->store_url_to_file($url);
    $self->progress("downloaded latest release for GitHub repo $owner/repo");

    my $json_text = File::Slurper::read_text($latest_json);
    my $latest_scalar = decode_json $json_text ;

    my $version ;
    if (exists($latest_scalar->{'tag_name'})) {
        $version = $latest_scalar->{'tag_name'} ;
    } else {
        croak "could not find tag_name in latest release of $owner/$repo";
    }

    unlink $latest_json ;

    return $version;
}

# Return:
#
#  -1: $v1 < $v2 ($v1 has LOWER  Debian version value than $v2)
#   1: $v1 > $v2 ($v1 has HIGHER Debian version value than $v2)
#   0: $v1 = $v2 ($v1 has SAME   Debian version value as   $v2)
sub compare_versions {
    my ($v1, $v2) = @_ ;
    my $a = AptPkg::Config->new();
    return $a->system->versioning->compare($v1, $v2) ;
    #return $AptPkg::Config::_config->system->versioning->compare($v1, $v2) ;
}

# Return the "difference" of two semantic versions.
#
# v3 = subtract_version(v1, v2)
#
# if v1 < v2 raise an error
# otherwise, take the difference of each component.
#
# Examples:
#
# subtract_version('1', '2')         --> ERROR
# subtract_version('2', '1')         --> '1'
# subtract_version('3', '1')         --> '2'
# subtract_version('3', '1.1')       --> ERROR
# subtract_version('3.1', '1')       --> ERROR
# subtract_version('3.1', '2.1')     --> '1.*'
# subtract_version('3.1', '2.9')     --> '1.*'
# subtract_version('3.1.1', '2.9')   --> ERROR
# subtract_version('3.1.1', '2.9.1') --> '1.*.*'
# subtract_version('3.2.1', '3.1.0') --> '0.1.*'
# subtract_version('3.2.1', '3.1.9') --> '0.1.*'
# subtract_version('3.2.1', '3.2.0') --> '0.0.1'
# subtract_version('5.2.1', '3.2.0') --> '2.*.*'

sub subtract_version {
    my ($v1, $v2) = @_ ;

    if (compare_versions($v1, $v2) < 0) {
        croak 'first argument is less than second argument' ;
    }

    my @c1 = split(/[.]/xsm, $v1);
    my @c2 = split(/[.]/xsm, $v2);

    if (scalar(@c1) != scalar(@c2)) {
        croak "'$v1' and '$v2' are not comparable";
    }

    my $length = scalar(@c1);
    # Iterate through the components starting from the right.
    my @diff = ();
    my $remaining_zero = 0;
    foreach my $i (0 .. $length - 1) {
        my $x1 = $c1[$i] + 0;
        my $x2 = $c2[$i] + 0;

        if ($remaining_zero) {
            push(@diff, q{*}) ;
        } elsif ($x1 > $x2) {
            push(@diff, $x1 - $x2) ;
            $remaining_zero = 1;
        } elsif ($x1 == $x2) {
            push(@diff, '0') ;
        } else {
            croak "should never get here ($x1,$x2)";
        }
    }

    use Data::Dumper ;

    my $diff_ver = join(q{.}, @diff) ;

    return $diff_ver;
}

# compare_diffed_versions('1', '2') --> -1
# compare_diffed_versions('2', '1') -->  1
# compare_diffed_versions('2', '2') -->  0
# compare_diffed_versions('2.*', '3.*')     -->  -1
# compare_diffed_versions('0.1.*', '0.2.*') -->  -1
# compare_diffed_versions('0.1.*', '0.0.1') -->   1
# compare_diffed_versions('3.*.*', '0.1.*') -->   1
# compare_diffed_versions('3.*.*', '2.*.*') -->   1
# compare_diffed_versions('0.0.1.*', '0.0.3.*') -->  -1

# Note that a diffed version has these properties:
#
#  1. Never starts with a '*'.
#  2. All positions to the right of a non-zero are '*'s.
#  3. All positions to the left of a number not zero are '0's.

sub compare_diffed_versions {
    my ($d1, $d2) = @_ ;

    my @c1 = split(/[.]/xsm, $d1);
    my @c2 = split(/[.]/xsm, $d2);

    my $length = scalar(@c1);
    foreach my $i (0 .. $length - 1) {
        my $x1 = $c1[$i] ;
        my $x2 = $c2[$i] ;

        # Step 1. Compare as strings. If the same, keep going.
        if ($x1 eq $x2) {
            next ;
        }

        # Step 2. They are different. If both numbers, compare and return.
        if (($x1 =~ m{^\d+$}xsm) && ($x2 =~ m{^\d+$}xsm)) {
            if ($x1 + 0 > $x2 + 0) {
                return 1 ;
            } elsif ($x1 + 0 < $x2 + 0) {
                return -1 ;
            } else {
                next ;
            }
        }

        # Step 3. If we get here we know that at least one of them is an
        # '*'.
        croak 'should never get here' ;
    }
}



1;
