use strict ;
use warnings ;

use Test::More tests => 33 ;
use English ;

use Stanford::DebRepo ;

# Make a Stanford::DebRepo object but point to a fake
# URL as all we want to do is test Packages parsing
my $dr = Stanford::DebRepo->new(
        url       => 'https://bogus.bogus.bogus',
        code      => 'buster',
        component => 'mail',
        ) ;

# Set the packages path to the patchman Packages file.
$dr->packages_path('./t/Packages-patchman') ;

# Get the patchman versions.
my @versions = $dr->get_versions('patchman-client') ;

ok (scalar(@versions) == 1,   'one version in Packages') ;
ok ($versions[0] eq '1.2.12', 'correct version found') ;

ok (Stanford::DebRepo::compare_versions('1.2', '1.2') == 0,
  'version compare says equal') ;
ok (Stanford::DebRepo::compare_versions('1.2', '1.3') < 0,
  'version compare says less than') ;
ok (Stanford::DebRepo::compare_versions('1.3', '1.2') > 0,
  'version compare says less than') ;
ok (Stanford::DebRepo::compare_versions('1.3.1', '1.3') > 0,
  'version compare says greater than (minor version)') ;

my ($diff, $at_error) ;
eval {
     $diff = Stanford::DebRepo::subtract_version('1', '2')
} ;
$at_error = $EVAL_ERROR;
ok ($at_error =~ m{^.*first[ ]argument[ ]is[ ]less[ ]than[ ]second[ ]argument.*$}xsm) ;

ok (Stanford::DebRepo::subtract_version('2', '1') eq '1') ;
ok (Stanford::DebRepo::subtract_version('3', '1') eq '2') ;
ok (Stanford::DebRepo::subtract_version('19.37', '14.1')    eq '5.*') ;
ok (Stanford::DebRepo::subtract_version('0.2.8', '0.1.200') eq '0.1.*') ;
ok (Stanford::DebRepo::subtract_version('3.1.4', '3.1.1')   eq '0.0.3') ;
ok (Stanford::DebRepo::subtract_version('3.1.2', '2.9.7')   eq '1.*.*') ;
ok (Stanford::DebRepo::subtract_version('3.9.2', '2.1.7')   eq '1.*.*') ;
ok (Stanford::DebRepo::subtract_version('3.2.1', '3.1.0')   eq '0.1.*') ;
ok (Stanford::DebRepo::subtract_version('3.2.1', '3.1.9')   eq '0.1.*') ;
ok (Stanford::DebRepo::subtract_version('5.6.9', '3.4.2')   eq '2.*.*') ;
ok (Stanford::DebRepo::subtract_version('5.6.9', '3.9.2')   eq '2.*.*') ;
ok (Stanford::DebRepo::subtract_version('5.6.9', '5.6.9')   eq '0.0.0') ;


ok (Stanford::DebRepo::compare_diffed_versions('1', '2') == -1) ;
ok (Stanford::DebRepo::compare_diffed_versions('2', '1') == 1);
ok (Stanford::DebRepo::compare_diffed_versions('2', '2') == 0) ;
ok (Stanford::DebRepo::compare_diffed_versions('2.*', '3.*') == -1) ;
ok (Stanford::DebRepo::compare_diffed_versions('3.*', '2.*') == 1) ;
ok (Stanford::DebRepo::compare_diffed_versions('3.*', '3.*') == 0) ;
ok (Stanford::DebRepo::compare_diffed_versions('0.0.1', '0.0.1') == 0) ;
ok (Stanford::DebRepo::compare_diffed_versions('0.0.1', '0.0.2') == -1) ;
ok (Stanford::DebRepo::compare_diffed_versions('0.1.*', '0.2.*') == -1) ;
ok (Stanford::DebRepo::compare_diffed_versions('0.1.*', '0.0.1') == 1) ;
ok (Stanford::DebRepo::compare_diffed_versions('3.*.*', '0.1.*') == 1) ;
ok (Stanford::DebRepo::compare_diffed_versions('3.*.*', '2.*.*') == 1) ;
ok (Stanford::DebRepo::compare_diffed_versions('0.0.1.*', '0.0.3.*') == -1) ;
ok (Stanford::DebRepo::compare_diffed_versions('0.0.10.*', '0.0.3.*') == 1) ;

